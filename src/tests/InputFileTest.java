package tests;

import Controller.A4JobList;
import Controller.JobPrinter;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


/**
 * Basic tests with different input files to check whether it calculates the total costs properly or not.
 */
public class InputFileTest
{
    @Test
    public void testFile1() throws Exception
    {
        String result = new JobPrinter().printJobList(new A4JobList().readJobList("printjobs.csv"));
        assertEquals("64.1", result);
    }

    @Test
    public void testFile2() throws Exception
    {
        String result = new JobPrinter().printJobList(new A4JobList().readJobList("jobs2.csv"));
        assertEquals("59", result);
    }

    @Test
    public void testFile3() throws Exception
    {
        String result = new JobPrinter().printJobList(new A4JobList().readJobList("jobs3.csv"));
        assertEquals("40", result);
    }

    @Test
    public void testFile4() throws Exception
    {
        String result = new JobPrinter().printJobList(new A4JobList().readJobList("jobs4.csv"));
        assertEquals("59.3", result);
    }

    @Test
    public void testFile5() throws Exception
    {
        String result = new JobPrinter().printJobList(new A4JobList().readJobList("jobs5.csv"));
        assertEquals("230", result);
    }

}