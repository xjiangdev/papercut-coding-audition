import Controller.A4JobList;
import Controller.JobPrinter;

public class Main
{
    public static void main (String[] arg)
    {

        //Attempt to use the file specified by the user
        String filename;

        try
        {
            filename = arg[0];
        }
        //No arguments found, use default filename
        catch (ArrayIndexOutOfBoundsException e)
        {
            filename = "printjobs.csv";
        }

        // Read the file and then print the job list
        new JobPrinter().printJobList(new A4JobList().readJobList(filename));
    }
}