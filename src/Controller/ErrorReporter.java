package Controller;

/**
 * Error reporting class that allows other classes to notify when something has gone wrong.
 *
 * The reason why I have chosen to use a Singleton is because it is a class that doesn't need multiple copies,
 * is relatively lightweight (As currently it just prints Strings) and also will be needed to be used in more than one
 * place once the program is expanded upon.
 *
 * The class is it's separate entity and isn't embedded into one of the other classes so it can be reused and if the
 * view changes (Such as if a GUI is added) then it can easily be modified to allow for that rather than going in to
 * each of the other classes, finding parts where error messages are thrown and changing the code in many different
 * paces.
 */
public class ErrorReporter
{
    private static ErrorReporter errorReporter;
    public static ErrorReporter getInstance()
    {
        // Only make a new object if an error is actually thrown
        if (errorReporter == null)
            errorReporter = new ErrorReporter();

        return errorReporter;
    }

    private ErrorReporter() {}

    public void printIOException()
    {
        System.out.println("Error: could not read input file");
    }

    public void printInvalidPrintingQuantity()
    {
        System.out.println("Error: Invalid quantities of prints");
    }

    public void printInvalidDoubleSidedValue()
    {
        System.out.println("Error: Double sided print not specified properly. Must be true or false");
    }

    public void printColouredAmtBiggerThanTotal()
    {
        System.out.println("Error: Amount of coloured prints must be smaller or the same amount as total prints");
    }

    public void printJobIncorrectFormat()
    {
        System.out.println("Error: Format of the job list is incorrect");
    }


}
