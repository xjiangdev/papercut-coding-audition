package Controller;

import Model.A4Job;
import Model.JobAbstract;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that provides functionality to read the job file and create Lists of job objects. Since only one paper type
 * is supported, the class is quite simple.
 *
 * If new paper sizes were to be introduced, it would be ideal to dynamically load a different file parser depending on
 * what paper size has been selected (Assuming the parameters are different, if not the same parser can be used).
 * This could possibly be achieved through a combination of the Factory design pattern and the Command design pattern.
 * An example of how this could be done is through the following process:
 *
 * 1. A variable is passed that flags what type of job list needs to be processed.
 * 2. Variable is passed into a factory class that returns a Command object. (There would be one object per paper size,
 * the class would conform to an interface)
 * 3. The filename is passed into the Command object and it is processed. This approach also allows multiple job lists
 * and files to be batch processed as they all use the same interface.
 */
public class A4JobList
{
    public List readJobList(String filename)
    {
        BufferedReader br;
        String jobString;
        String jobListDelimiter = ",";
        /*
         * An ArrayList was chosen as it is the most simple to work with and the drawback of how ArrayLists are
         * expanded to cater for large amounts of entries under the hood will most likely not be an issue in
         * this use case.
         */
        List<JobAbstract> A4JobList = new ArrayList<>();

        try
        {
            /*
             * Read the input file one line at a time with a comma as the delimiter. Once the line has been split
             * up it is stored as a String array and then used to create a new job object.
             */
            br = new BufferedReader(new FileReader(filename));
            while ((jobString = br.readLine()) != null)
            {
                String[] jobStringArray = jobString.split(jobListDelimiter);

                // Attempt to create a new A4Job, if successful then add it to the list.
                A4Job A4Job = new A4Job();
                if (createA4Job(A4Job, jobStringArray))
                {
                    A4JobList.add(A4Job);
                }
                // If an error was found, clear the List and do not print the job list
                else
                {
                    A4JobList.clear();
                    break;
                }

            }

        }
        // Could not read the file or file contents
        catch (IOException e)
        {
            ErrorReporter.getInstance().printIOException();
        }

        return A4JobList;
    }

    /**
     * Attempts to build a new A4Job object with correct information. The input validation for the job is done here.
     *
     * @param A4Job An empty job object
     * @param jobStringArray Array containing the input information
     * @return Boolean value for whether creation was successful or not
     */
    private boolean createA4Job(A4Job A4Job, String[] jobStringArray)
    {
        if (jobStringArray.length != 3)
        {
            ErrorReporter.getInstance().printJobIncorrectFormat();
            return false;
        }


        int totalPages;
        int colourPages;
        boolean isDoubleSided;

        try
        {
            /*
             * Attempts to parse the page amounts as numbers. If they are not numbers then a NumberFormatException will
             * be thrown and validation will fail. Trim() is used to remove any whitespace.
             */
            totalPages = Integer.parseInt(jobStringArray[0].trim());
            colourPages = Integer.parseInt(jobStringArray[1].trim());

            // Real world constraint
            if (totalPages < colourPages)
            {
                ErrorReporter.getInstance().printColouredAmtBiggerThanTotal();
                return false;
            }

        }
        catch (NumberFormatException e)
        {
            ErrorReporter.getInstance().printInvalidPrintingQuantity();
            return false;
        }

        // Parse the true/false
        if (jobStringArray[2].trim().equalsIgnoreCase("TRUE"))
            isDoubleSided = true;
        else if (jobStringArray[2].trim().equalsIgnoreCase("FALSE"))
            isDoubleSided = false;
        else
        {
            ErrorReporter.getInstance().printInvalidDoubleSidedValue();
            return false;
        }

        // Successfully create the job object
        A4Job.setJobDetails(totalPages, colourPages, isDoubleSided);
        return true;
    }
}
