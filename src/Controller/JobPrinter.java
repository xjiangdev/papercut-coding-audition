package Controller;

import Model.JobAbstract;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;

/**
 * Class that prints the job details to the console. Currently only supports A4 print jobs.
 *
 * Printing the job details is separated from the job objects as the View may change. If the print method was put
 * inside the job object, it couples the printing to console only. If multiple different job objects exist and the
 * GUI changes then all the different types of job objects would need to be rewritten to cater for the change in View.
 */
public class JobPrinter
{
    public String printJobList(List<JobAbstract> JobList)
    {
        if (JobList.size() == 0)
            return "0";

        BigDecimal totalCost = new BigDecimal(0);

        /*
         * Main loop for printing each job in the job list. A Stringbuilder is used as it uses less memory than if you
         * concatenated multiple Strings in a sequence.
         */
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("A4 Print job: \n");
        for (int i = 0; i < JobList.size(); i++)
        {

            JobAbstract currentJob = JobList.get(i);
            stringBuilder.append(i+1);
            stringBuilder.append(". Total pages: ");
            stringBuilder.append(currentJob.getTotalPageAmt());
            stringBuilder.append(". Coloured pages: ");
            stringBuilder.append(currentJob.getColourAmt());
            stringBuilder.append(". Is double sided? ");
            if (currentJob.getIsDoubleSided())
                stringBuilder.append("Yes");
            else
                stringBuilder.append("No");

            stringBuilder.append(". Total cost: $");
            BigDecimal costInDollars = currentJob.getTotalCost().divide(new BigDecimal(100));
            totalCost = costInDollars.add(totalCost);
            stringBuilder.append(costInDollars);
            System.out.println(stringBuilder.toString());
            stringBuilder.setLength(0);
        }

        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        stringBuilder.setLength(0);
        stringBuilder.append("Total cost: ");
        stringBuilder.append(numberFormat.format(totalCost));
        System.out.println(stringBuilder.toString());

        return totalCost.stripTrailingZeros().toPlainString();
    }
}
