package Model;

import java.math.BigDecimal;

/**
 * An abstract job that other classes can use to ensure consistency when dealing with Job classes.
 *
 * Despite this code audition only supporting one type of paper size, having this abstract class will allow support for
 * multiple sizes in the future.
 */
public interface JobAbstract
{
    int getTotalPageAmt();

    int getColourAmt();

    boolean getIsDoubleSided();

    /**
     * The total cost of the entire Job after considering page costs and the amount of pages printed.
     *
     * @return returns the Job cost in cents
     */
    BigDecimal getTotalCost();
}
