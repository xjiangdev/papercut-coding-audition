package Model;

import java.math.BigDecimal;

/**
 * Concrete implementation of a Job for A4 sheets of paper.
 */
public class A4Job implements JobAbstract
{
    /*
     * Store the cost of coloured and black and white prints in cents. Storing the costs as an BigDecimal also allows
     * better handling of number precision and is typically recommended for money.
     */
    private BigDecimal colourcost = new BigDecimal(0);
    private BigDecimal bwcost = new BigDecimal(0);
    private boolean isDoubleSided = false;

    // Amount of coloured and black and white prints.
    private int totalPageAmt = 0;
    private int colourAmt = 0;

    public A4Job () {}

    public void setJobDetails(int totalPageAmt, int colourAmt, boolean isDoubleSided)
    {
        this.totalPageAmt = totalPageAmt;
        this.colourAmt = colourAmt;
        this.isDoubleSided = isDoubleSided;

        /*
         * Set the prices of the pages depending on double sided or not. It might be a good idea to put the prices in
         * an external file such as a JSON or XML file for easy modification in the future.
         */
        if (this.isDoubleSided)
        {
            colourcost = new BigDecimal(20);
            bwcost = new BigDecimal(10);
        }
        else
        {
            colourcost = new BigDecimal(25);
            bwcost = new BigDecimal(15);
        }
    }

    @Override
    public int getTotalPageAmt()
    {
        return totalPageAmt;
    }

    @Override
    public int getColourAmt()
    {
        return colourAmt;
    }

    @Override
    public boolean getIsDoubleSided()
    {
        return isDoubleSided;
    }

    @Override
    public BigDecimal getTotalCost()
    {
        BigDecimal totalColourCost = colourcost.multiply(new BigDecimal(colourAmt));
        BigDecimal totalBwCost = bwcost.multiply(new BigDecimal((totalPageAmt - colourAmt)));

        return totalBwCost.add(totalColourCost);
    }
}
