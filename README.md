# README #

Some of the main goals of my design were to make the code extensible in the future and to make sure there was low coupling between classes. This is because while the current problem description is basic, future additions can be forecasted so it would be wise to prepare for them. Despite this I also chose to keep the solution in scope as much as possible without compromising the previously mentioned design goals too much.

The code was written with as much care as possible to avoid combining the model/program logic with the View representation of the program, Which in this case is Console output. I chose to do this as it would be no different from coding references to specific GUI elements in a program with a real GUI.

In depth comments of specific code sections and design decisions are written as comments in the class files.

The code was written with the IntelliJ IDE and uses TestNG for the unit testing as it came with the IDE. 

Possible extensions/improvements:

Move the prices to an external JSON/XML file

Use a build automation system such as Gradle

Write more tests for testing failure states

Use JUnit instead of TestNG

Refactor code to cater for different types of paper sizes (Was done to an extent)
